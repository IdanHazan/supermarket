import { useEffect, useState } from "react";
import * as d3 from "d3";
import axios from "axios";

export const TopProductsChartD3 = () => {
    const API_URL = "http://localhost:8080/api/carts/TopProducts";
    let loaded=false
    const [data, setData] = useState([]);

    useEffect(() => {
        axios.get(API_URL).then((data) => setData(data.data));
    }, []);

    useEffect(() => {
        if (data.length > 0 && !loaded) {
            loaded = true;
            console.log('galmo now try');
            graph_products_ratings(data)
            setData([]);
        }
    }, [data[0]]);


    return (
        <div id="TopProductsChartD3"></div>
    );
}
function graph_products_ratings(data) {
    const margin = { top: 10, right: 10, bottom: 30, left: 10 };
    const width = 1200 - margin.left - margin.right;
    const height = 400 - margin.top - margin.bottom;

    const svg = d3
        .select("#TopProductsChartD3")
        .append("svg")
        .attr("width", width - margin.left - margin.right)
        .attr("height", height - margin.top - margin.bottom)
        .attr("viewBox", [0, 0, width, height]);

    const x = d3
        .scaleBand()
        .domain(d3.range(data.length))
        .range([margin.left, width - margin.right])
        .padding(0.1);

    const y = d3
        .scaleLinear()
        .domain([0, 10])
        .range([height - margin.bottom, margin.top]);
    svg
        .append("g")
        .selectAll("rect")
        .data(data)
        .join("rect")
        .attr("fill", (d) => {
            return "green";
        })
        .attr("x", (d, i) => x(i))
        .attr("y", (d) => y(d.count))
        .attr("title", (d) => d.count)
        .attr("class", "rect")
        .attr("height", (d) => y(0) - y(d.count))
        .attr("width", x.bandwidth());

    svg
        .append("text")
        .attr("x", width / 2)
        .attr("y", margin.top / 2 + 10)
        .attr("text-anchor", "middle")
        .style("font-size", "16px")
        .style("text-decoration", "underline")
        .text("Top Sold Products");

    // creating names for the axis
    function yAxis(g) {
        g.attr("transform", `translate(${margin.left}, 0)`)
            .call(d3.axisLeft(y).ticks(null, data.format))
            .attr("font-size", "15px");
    }
    function xAxis(g) {
        g.attr("transform", `translate(0,${height - margin.bottom})`)
            .call(d3.axisBottom(x).tickFormat((i) => data[i]._id))
            .attr("font-size", "15px");
    }

    svg.append("g").call(xAxis);
    svg.append("g").call(yAxis);
    svg.node();
}