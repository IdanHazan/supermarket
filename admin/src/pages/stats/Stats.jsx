import "./Stats.css";
import { PieProductsChart } from "../../components/charts/PieProducts";
import { TopProductsChartD3 } from "../../components/charts/TopProductsD3";

const StatsPage = () => {
  return (
    <div className="statsPage">
      <center>
        <b>
          <h1>Statistics</h1>
        </b>
      </center>
      <h3>Top 10 Products</h3>
      <TopProductsChartD3 />
      <h3>Departments Pie Chart</h3>
      <PieProductsChart />
    </div>
  );
};

export default StatsPage;
