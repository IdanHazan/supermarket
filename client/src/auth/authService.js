import axios from "axios";
import * as firebaseService from "./firebase"
const API_URL = "/api/clients/";

// Register client
const register = async (clientData) => {
  await firebaseService.firebaseCreateUser(clientData.email, clientData.password)

  const response = await axios.post(API_URL, clientData);

  if (response.data) {
    localStorage.setItem("client", JSON.stringify(response.data));
  }

  return response.data;
};

// Login client
const login = async (clientData) => {
  await firebaseService.firebaseSignIn(clientData.email, clientData.password)
  
  const response = await axios.post(API_URL + "login", clientData);

  if (response.data) {
    localStorage.setItem("client", JSON.stringify(response.data));
  }


  return response.data;
};

// Logout user
const logout = () => {
  localStorage.removeItem("client");
  
};

const authService = {
  register,
  logout,
  login,
};

export default authService;
