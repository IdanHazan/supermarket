// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
import { getAuth, createUserWithEmailAndPassword, signInWithEmailAndPassword } from "firebase/auth";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
    apiKey: "AIzaSyBDqRzb6vJUP9F2ZQtMNkOMUff_Vp5kEQ4",
    authDomain: "supermarket-users.firebaseapp.com",
    projectId: "supermarket-users",
    storageBucket: "supermarket-users.appspot.com",
    messagingSenderId: "588295189037",
    appId: "1:588295189037:web:55153f1a5ea7e2e2effea4",
    measurementId: "G-XH13NDMK78"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const auth = getAuth(app);
const analytics = getAnalytics(app);

export const firebaseCreateUser = async (email, password) => {
    return createUserWithEmailAndPassword(auth, email, password)
        .then((userCredential) => {
            // Signed in 
            const user = userCredential.user;
            return user;
        })
        .catch((error) => {
            const errorCode = error.code;
            const errorMessage = error.message;
            throw error;
        });
}

export const firebaseSignIn = async (email, password) => {
    return signInWithEmailAndPassword(auth, email, password)
        .then((userCredential) => {
            // Signed in 
            const user = userCredential.user;

            return user;
        })
        .catch((error) => {
            const errorCode = error.code;
            const errorMessage = error.message;
            throw error;
        });
}

const firebaseService = {
    firebaseCreateUser,
    firebaseSignIn
}

export default firebaseService;