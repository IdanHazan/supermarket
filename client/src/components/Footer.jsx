import React from "react";
import "../styles/Footer.css";
import {
  AiFillPhone,
  AiFillFacebook,
  AiFillPrinter,
  AiFillHome,
  AiFillMail,
} from "react-icons/ai";

function Footer() {
  return (
		<footer className="footer-distributed footer">

<div className="footer-right">

	<a href="#"><i className="fa fa-cloud"></i></a>
	<a href="#"><i className="fa fa-wand-magic-sparkles"></i></a>
	<a href="#"><i className="fa fa-camera-retro"></i></a>
	<a href="#"><i className="fa fa-bell"></i></a>

</div>

<div className="footer-left">

	<p className="footer-links">
		<a className="link-1" href="#">Home</a>

		<a href="#">Pricing</a>

		<a href="#">About</a>

		<a href="#">Contact</a>
	</p>

	<p>MySupermarket &copy; 2023</p>
</div>

</footer>
  );
}

export default Footer;
