import { useState, useEffect } from "react";
import { FaSignInAlt } from "react-icons/fa";
import { useSelector, useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { login, reset } from "../auth/authSlice";
import Spinner from "../components/Spinner";


function Login() {
  const [formData, setFormData] = useState({
    email: "",
    password: "",
  });

  const { email, password } = formData;

  const navigate = useNavigate();
  const dispatch = useDispatch();

  const { client, isLoading, isError, isSuccess, message } = useSelector(
    (state) => state.auth
  );

  useEffect(() => {
    if (isError) {
      alert(message);
    }

    if (isSuccess || client) {
      navigate("/");
    }

    dispatch(reset());
  }, [client, isError, isSuccess, message, navigate, dispatch]);

  const onChange = (e) => {
    setFormData((prevState) => ({
      ...prevState,
      [e.target.name]: e.target.value,
    }));
  };

  const onSubmit = (e) => {
    e.preventDefault();

    const clientData = {
      email,
      password,
    };

    dispatch(login(clientData));
  };

  if (isLoading) {
    return <Spinner />;
  }

  return (
    <>
    <div style={{display: "flex", flexDirection: "column", alignItems: "center"}}>
    <div className="header" style={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "rgb(67 125 175)",
        color: "#ffffff",
        height: "10vh",
        width: "80vh"
      }}>
      <h1 style={{ fontSize: "36px", fontWeight: "bold" }}>Login and start shopping</h1>
    </div>

    <div className="form" style={{
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      width: "80vh",
      // marginBottom: "20%"
    }}>
    <form onSubmit={onSubmit} style={{
      display: "flex",
      flexDirection: "column",
      alignItems: "center",
      width: "100%"
    }}>
    <div className="form-group" style={{
      width: "100%",
      display: "flex",
      justifyContent: "center",
    }}>
      <input
        type="email"
        className="input-design"
        id="email"
        name="email"
        value={email}
        placeholder="Enter your email"
        onChange={onChange}
        style={{
          padding: "10px",
          fontSize: "16px",
          borderRadius: "5px",
          border: "1px solid #5ba5e5"
        }}
      />
    </div>
    <div className="form-group" style={{
      width: "100%",
      display: "flex",
      justifyContent: "center",
    }}>
      <input
        type="password"
        className="input-design"
        id="password"
        name="password"
        value={password}
        placeholder="Enter password"
        onChange={onChange}
        style={{
          padding: "10px",
          fontSize: "16px",
          borderRadius: "5px",
          border: "1px solid #5ba5e5"
        }}
      />
    </div>
    <div className="form-group" style={{
      display: "flex",
      justifyContent: "center",
      margin: "10px 0"
    }}>
      <button type="submit" className="submit-btn" style={{
        padding: "10px 20px",
        backgroundColor: "#5ba5e5",
        color: "#ffffff",
        fontWeight: "bold",
        borderRadius: "5px",
        border: "none",
        cursor: "pointer"
      }}>
        Submit
      </button>
    </div>
  </form>
</div>
    </div>
      
    </>
  );
}

export default Login;
