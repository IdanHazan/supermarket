import "../styles/NavBar.css";
import { FaSignInAlt, FaSignOutAlt, FaUser } from "react-icons/fa";
import ChatIcon from "@mui/icons-material/Chat";
import ShopIcon from "@mui/icons-material/Shop";
import StoreIcon from "@mui/icons-material/Store";
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import { Link, useNavigate } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { logout, reset } from "../auth/authSlice";
import { clearCart } from '../redux/actions/cartActions'
import logo from "../assets/logo.jpg";

const NavBar = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const { client } = useSelector((state) => state.auth);

  const onLogout = () => {
    dispatch(logout());
    dispatch(clearCart())
    dispatch(reset());
    navigate("/");
  };

  const cart = useSelector((state) => state.cart);
  const { cartItems } = cart;
  const getCartCount = () => {
    return cartItems.reduce((qty, item) => Number(item.qty) + qty, 0);
  };
  return (
    <div id="navbar">
      <div className="nav-wrapper">
        <div className="logo">
          <a href="/"><StoreIcon /> My Supermarket</a>

        </div>
        <ul id="menu">
          {client ? (
            <>
              <li>
                <Link to="/cart">
                  {" "}
                  <ShoppingCartIcon />
                  <span>
                    Cart <span>{getCartCount()}</span>
                  </span>
                </Link>
              </li>
              <li>
                <Link to="/shop">
                  {" "}
                  <ShopIcon />Shop
                </Link>
              </li>
              <li>
                <Link to="/chat">
                  {" "}
                  <ChatIcon /> chat
                </Link>
              </li>
              <li>
                <a onClick={onLogout}>
                  <FaSignOutAlt /> Logout
                </a>
              </li>
            </>
          ) : (
            <>
              <li>
                <Link to="/login">
                  <FaSignInAlt />
                  {" "}
                  <span>Login</span>
                </Link>
              </li>
              <li>
                <Link to="/register">
                  <FaUser />
                  {" "}
                  <span>Register</span>
                </Link>
              </li>
            </>
          )}
        </ul>
      </div>
    </div>
  );
};

export default NavBar;

    // <nav className="navbar">
    //   {/* logo */}
    //   <div className="navbar__logo">
    //     <Link style={{ textDecoration: "none" }} to="/">
    //       <img src={logo} style={{ height: "5vh" }} />
    //       <h2>My Supermarket</h2>
    //     </Link>
    //   </div>
    //   {/* links */}
    //   <ul className="navbar__links">
        // {client ? (
        //   <>
        //     <li>
        //       <Link to="/cart" className="cart_link">
        //         {/* Icon */}
        //         <i className="fas fa-cart-plus"></i>
        //         <span>
        //           Cart <span className="cartlogo__badge">{getCartCount()}</span>
        //         </span>
        //       </Link>
        //     </li>
        //     <li>
        //       <Link to="/shop">Shop</Link>
        //     </li>
        //     <li>
        //       <Link to="/chat">
        //         {" "}
        //         <ChatIcon /> chat
        //       </Link>
        //     </li>
        //     <li>
        //       <button className="btn btn-primary" onClick={onLogout}>
        //         <FaSignOutAlt /> Logout
        //       </button>
        //     </li>
        //   </>
        // ) : (
        //   <>
        //     <li>
        //       <Link to="/login">
        //         <FaSignInAlt />
        //         <span>Login</span>
        //       </Link>
        //     </li>
        //     <li>
        //       <Link to="/register">
        //         <FaUser />
        //         <span>Register</span>
        //       </Link>
        //     </li>
        //   </>
        // )}
    //   </ul>
    // </nav>