const mongooose = require("mongoose");

const connectDB = async () => {
  try {

    // const conn = await mongooose.connect(process.env.MONGO_URI);
    const conn = await mongooose.connect("mongodb+srv://mongodb:mongodb@cluster0.od5ccku.mongodb.net/?retryWrites=true&w=majority");

    console.log(
      `MongoDB Connected: ${conn.connection.host}`.cyan.underline.bold
    );
  } catch (error) {
    console.log(error);
    process.exit(1);
  }
};

module.exports = connectDB;
